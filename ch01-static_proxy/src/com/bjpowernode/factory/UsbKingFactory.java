package com.bjpowernode.factory;

import com.bjpowernode.service.UsbSell;

/**
 * 目标类，金士顿,不接受用户的单独购买。
 */
public class UsbKingFactory implements UsbSell {
    @Override
    public float sell(int amount) {
       //一个128u盘是85元.
        //后期根据amount，可以实现不同的价格。
        return 85.0f;
    }
}
